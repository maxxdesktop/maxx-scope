# MaXX Scope

MaXX Scope is a Smart Application and Service Orchestration for multi-core system that supports CPU core partitioning for maximum performance and efficiency. With MaXX Scope, a system's CPU cores can be allocated, grouped or reserved to execute/run p